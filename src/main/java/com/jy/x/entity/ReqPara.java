package com.jy.x.entity;

import lombok.Data;

/**
 * @program: x
 * @author: Jy
 * @create: 2019-08-09 14:14
 **/
@Data
public class ReqPara {

    private String sql;
    private String packageName;
}
